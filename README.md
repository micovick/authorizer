# Projeto Zitrus - Autorizador

## Tecnologias

- Spring boot framework
- Banco de dados em memória H2
- JSP
- Docker
- Jboss Wildfly
- Arquitetura hexagonal

## Guia de execução do projeto


Para executar o projeto no Docker, abra um terminal(Windows) siga os seguintes passos:

1. `./mvnw package`
2. `docker build -t springio/authorizer .`
3. `docker run -p 8080:8080 -p 9990:9990 springio/authorizer`

Após a execução dos comandos acesse pelo navegador a url: [http://localhost:8080/authorizer/](http://localhost:8080/authorizer/)

# Licença

Este projeto está licenciado nos termos da licença MIT.