<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>Solicitações de Procedumentos</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    </head>
    <body id="page-top" data-bs-spy="scroll" data-bs-target="#mainNav">
        <nav id="mainNav" class="navbar navbar-light navbar-expang-lg sticky-top bg-secondary text-uppercase mb-5">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#page-top">Zitrus</a>
                </div>
            </div>
        </nav>
        <header class="d-lg-flex masthead">
            <div class="container mt-10">
                <button type="button" class="btn btn-dark mb-2" onclick="location.href ='/authorizer/solicitar'">Solicitar</button>
                <table class="table table-dark table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Cód.</th>
                            <th scope="col">Idade</th>
                            <th scope="col" >Sexo</th>
                            <th scope="col" >Permitido</th>
                            <th scope="col" >Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${requestProcedures}" var="procedure">
                            <tr>
                                <td>${procedure.code}</td>
                                <td>${procedure.age}</td>
                                <td>${procedure.sex}</td>
                                <td>${procedure.allowed}</td>
                                <td>
                                    <c:url var="request_url" value="/remover"/>
                                    <form action="${request_url}/${procedure.id}" method="POST">
                                        <input type="hidden" name="id" value="${procedure.id}" />
                                        <button type="submit" class="btn btn-primary" name="remove">Remover</button>
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </header>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    </body>
</html>