<html>
  <head>
      <title>Solicitações de Procedumentos</title>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
  </head>
  <body id="page-top" data-bs-spy="scroll" data-bs-target="#mainNav">
    <nav id="mainNav" class="navbar navbar-light navbar-expang-lg sticky-top bg-secondary text-uppercase mb-5">
      <div class="container">
       <div class="navbar-header">
          <a class="navbar-brand" href="#page-top">Zitrus</a>
        </div>
      </div>
    </nav>
    <header class="d-lg-flex masthead">
      <div class="container mt-10">
        <c:if test="${addSuccess == true}">
            <div class="alert alert-success" role="alert">Cadastrado com sucesso</div>
        </c:if>
        <c:if test="${addError == true}">
            <div class="alert alert-danger" role="alert">Cadastrado com sucesso</div>
        </c:if>
        <c:url var="request_url" value="/solicitar"/>
        <form action="${request_url}" method="post" modelAttribute="procedureRequest">
          <div class="mb-3">
            <label for="code" class="form-label">Cód. do Procedimento</label>
            <input type="text" class="form-control" id="code" name="code" path="code">
          </div>
          <div class="mb-3">
            <label for="age" class="form-label">Idade do Paciente</label>
            <input type="text" class="form-control" id="age" name="age" path="age">
          </div>
          <div class="mb-3">
            <label for="sex" class="form-label">Sexo do Paciente</label>
            <select id="sex" name="sex" class="form-select" aria-label="Sexo" path="sex">
              <option selected>Selecione</option>
              <option value="M">Masculino</option>
              <option value="F">Feminino</option>
            </select>
          </div>
          <button type="button" class="btn btn-secondary" onclick="location.href ='/authorizer/'">Voltar</button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </form>

      </div>
    </header>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
  </body>
</html>