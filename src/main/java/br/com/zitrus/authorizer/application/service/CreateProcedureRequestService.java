package br.com.zitrus.authorizer.application.service;

import br.com.zitrus.authorizer.application.port.in.CreateProcedureRequestUseCase;
import br.com.zitrus.authorizer.application.port.out.CreateProcedureRequestPort;
import br.com.zitrus.authorizer.application.port.out.ProcedurePort;
import br.com.zitrus.authorizer.domain.Procedure;
import br.com.zitrus.authorizer.domain.ProcedureRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CreateProcedureRequestService implements CreateProcedureRequestUseCase {

    private final CreateProcedureRequestPort createProcedureRequestPort;
    private final ProcedurePort procedurePort;

    @Override
    public Optional<ProcedureRequest> create(CreateProcedureRequestCommand command) {
        ProcedureRequest procedureRequestCommand = command.getProcedureRequest();
        Optional<Procedure> optionalProcedure = procedurePort.getByCodeAndAgeAndSex(procedureRequestCommand.getCode(), procedureRequestCommand.getAge(), procedureRequestCommand.getSex());

        procedureRequestCommand.setAllowed(false);
        if (optionalProcedure.isPresent() && optionalProcedure.get().getAllow()) {
            procedureRequestCommand.setAllowed(true);
        }

        return createProcedureRequestPort.create(procedureRequestCommand);
    }
}
