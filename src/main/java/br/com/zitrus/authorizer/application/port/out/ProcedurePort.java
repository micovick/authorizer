package br.com.zitrus.authorizer.application.port.out;

import br.com.zitrus.authorizer.domain.Procedure;

import java.util.Optional;

public interface ProcedurePort {
    Optional<Procedure> getByCodeAndAgeAndSex(Integer code, Integer age, String sex);
}
