package br.com.zitrus.authorizer.application.port.out;

import br.com.zitrus.authorizer.domain.ProcedureRequest;

import java.util.List;
import java.util.Optional;

public interface ListProcedureRequestPort {
    Optional<ProcedureRequest> getById(Long id);

    List<ProcedureRequest> listAll();
}
