package br.com.zitrus.authorizer.application.service;

import br.com.zitrus.authorizer.application.port.in.ListProcedureRequestUseCase;
import br.com.zitrus.authorizer.application.port.out.ListProcedureRequestPort;
import br.com.zitrus.authorizer.domain.ProcedureRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ListProcedureRequestService implements ListProcedureRequestUseCase {

    private final ListProcedureRequestPort listProcedureRequestPort;

    @Override
    public Optional<ProcedureRequest> getById(Long id) {
        return listProcedureRequestPort.getById(id);
    }

    @Override
    public List<ProcedureRequest> listAll() {
        return listProcedureRequestPort.listAll();
    }
}
