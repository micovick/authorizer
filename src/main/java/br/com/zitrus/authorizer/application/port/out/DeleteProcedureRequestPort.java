package br.com.zitrus.authorizer.application.port.out;

public interface DeleteProcedureRequestPort {
    boolean delete(Long id);
}
