package br.com.zitrus.authorizer.application.service;

import br.com.zitrus.authorizer.application.port.in.DeleteProcedureRequestUseCase;
import br.com.zitrus.authorizer.application.port.out.DeleteProcedureRequestPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteProcedureRequestService implements DeleteProcedureRequestUseCase {
    private final DeleteProcedureRequestPort deleteProcedureRequestPort;
    @Override
    public boolean delete(DeleteProcedureRequestCommand deleteProcedureRequestCommand) {
        return deleteProcedureRequestPort.delete(deleteProcedureRequestCommand.getId());
    }
}
