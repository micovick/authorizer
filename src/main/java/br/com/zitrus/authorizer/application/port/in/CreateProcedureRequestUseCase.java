package br.com.zitrus.authorizer.application.port.in;

import br.com.zitrus.authorizer.domain.ProcedureRequest;
import br.com.zitrus.authorizer.utility.SelfValidating;
import com.sun.istack.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.Optional;

public interface CreateProcedureRequestUseCase {
    Optional<ProcedureRequest> create(CreateProcedureRequestCommand command);

    @Value
    @EqualsAndHashCode(callSuper = false)
    class CreateProcedureRequestCommand extends SelfValidating<CreateProcedureRequestCommand> {
        @NotNull
        ProcedureRequest procedureRequest;

        public CreateProcedureRequestCommand(ProcedureRequest procedureRequest) {
            this.procedureRequest = procedureRequest;
//            validateSelf();
        }
    }
}
