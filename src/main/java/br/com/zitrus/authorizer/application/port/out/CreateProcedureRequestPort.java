package br.com.zitrus.authorizer.application.port.out;

import br.com.zitrus.authorizer.domain.ProcedureRequest;

import java.util.Optional;

public interface CreateProcedureRequestPort {
    Optional<ProcedureRequest> create(ProcedureRequest procedureRequest);
}
