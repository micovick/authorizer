package br.com.zitrus.authorizer.application.port.in;

import br.com.zitrus.authorizer.domain.ProcedureRequest;

import java.util.List;
import java.util.Optional;

public interface ListProcedureRequestUseCase {
    Optional<ProcedureRequest> getById(Long id);

    List<ProcedureRequest> listAll();
}
