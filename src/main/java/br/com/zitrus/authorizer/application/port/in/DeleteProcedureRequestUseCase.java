package br.com.zitrus.authorizer.application.port.in;

import br.com.zitrus.authorizer.utility.SelfValidating;
import com.sun.istack.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Value;

public interface DeleteProcedureRequestUseCase {
    boolean delete(DeleteProcedureRequestCommand deleteProcedureRequestCommand);

    @Value
    @EqualsAndHashCode(callSuper = false)
    class DeleteProcedureRequestCommand extends SelfValidating<DeleteProcedureRequestCommand> {
        @NotNull
        Long id;

        public DeleteProcedureRequestCommand(Long id) {
            this.id = id;
//            validateSelf();
        }
    }
}
