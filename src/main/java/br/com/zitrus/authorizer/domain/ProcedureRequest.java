package br.com.zitrus.authorizer.domain;

import br.com.zitrus.authorizer.utility.SelfValidating;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@EqualsAndHashCode(callSuper = false)
public class ProcedureRequest extends SelfValidating<ProcedureRequest> {
    private Long id;

    @NotNull
    private Integer code;

    @NotNull
    private Integer age;

    @NotBlank
    private String sex;

    private boolean allowed;

    }
