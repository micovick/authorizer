package br.com.zitrus.authorizer.adapter.persistence.procedureRequest;

import br.com.zitrus.authorizer.adapter.persistence.procedureRequest.ProcedureRequestJpaEntity;
import br.com.zitrus.authorizer.domain.ProcedureRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ProcedureRequestPersistenceMapper {

    public ProcedureRequestJpaEntity mapToJpaEntity(ProcedureRequest procedureRequest) {
        ProcedureRequestJpaEntity procedureRequestJpaEntity = new ProcedureRequestJpaEntity();
        procedureRequestJpaEntity.setId(procedureRequest.getId());
        procedureRequestJpaEntity.setAge(procedureRequest.getAge());
        procedureRequestJpaEntity.setSex(procedureRequest.getSex());
        procedureRequestJpaEntity.setCode(procedureRequest.getCode());
        procedureRequestJpaEntity.setAllowed(procedureRequest.isAllowed());

        return procedureRequestJpaEntity;
    }

    public ProcedureRequest mapToDomain(ProcedureRequestJpaEntity procedureRequestJpaEntity) {
        ProcedureRequest procedureRequest = new ProcedureRequest();
        procedureRequest.setId(procedureRequestJpaEntity.getId());
        procedureRequest.setCode(procedureRequestJpaEntity.getCode());
        procedureRequest.setAge(procedureRequestJpaEntity.getAge());
        procedureRequest.setSex(procedureRequestJpaEntity.getSex());
        procedureRequest.setAllowed(procedureRequestJpaEntity.isAllowed());

        return procedureRequest;
    }

    public List<ProcedureRequest> mapToDomain(List<ProcedureRequestJpaEntity> entities) {
        return entities.stream().map(this::mapToDomain).collect(Collectors.toList());
    }
}
