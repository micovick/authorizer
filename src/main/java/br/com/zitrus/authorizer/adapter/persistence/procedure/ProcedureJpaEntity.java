package br.com.zitrus.authorizer.adapter.persistence.procedure;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@RequiredArgsConstructor
@Entity(name = "ProcedureJpaEntity")
@Table(name = "procedure")
public class ProcedureJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer code;

    private Integer age;

    private String sex;

    private boolean allow;
}
