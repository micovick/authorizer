package br.com.zitrus.authorizer.adapter.persistence.procedure;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProcedureRepository extends JpaRepository<ProcedureJpaEntity, Long> {
    Optional<ProcedureJpaEntity> findByCodeAndAgeAndSex(Integer code, Integer age, String sex);
}
