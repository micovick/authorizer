package br.com.zitrus.authorizer.adapter.persistence.procedureRequest;

import br.com.zitrus.authorizer.application.port.out.CreateProcedureRequestPort;
import br.com.zitrus.authorizer.application.port.out.DeleteProcedureRequestPort;
import br.com.zitrus.authorizer.application.port.out.ListProcedureRequestPort;
import br.com.zitrus.authorizer.domain.ProcedureRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Transactional
public class ProcedureRequestAdapter implements CreateProcedureRequestPort, DeleteProcedureRequestPort, ListProcedureRequestPort {

    private final ProcedureRequestRepository procedureRequestRepository;
    private final ProcedureRequestPersistenceMapper procedureRequestPersistenceMapper;

    @Override
    public Optional<ProcedureRequest> create(ProcedureRequest procedureRequest) {
        return Optional.of(procedureRequestPersistenceMapper.mapToDomain(procedureRequestRepository.save(procedureRequestPersistenceMapper.mapToJpaEntity(procedureRequest))));
    }

    @Override
    public boolean delete(Long id) {
        Optional<ProcedureRequestJpaEntity> optionalProcedureRequest = procedureRequestRepository.findById(id);

        if (optionalProcedureRequest.isPresent()) {
            procedureRequestRepository.delete(optionalProcedureRequest.get());
            return true;
        }

        return false;
    }

    @Override
    public Optional<ProcedureRequest> getById(Long id) {
        return procedureRequestRepository.findById(id).map(procedureRequestPersistenceMapper::mapToDomain);
    }

    @Override
    public List<ProcedureRequest> listAll() {
        return procedureRequestPersistenceMapper.mapToDomain(procedureRequestRepository.findAll());
    }
}
