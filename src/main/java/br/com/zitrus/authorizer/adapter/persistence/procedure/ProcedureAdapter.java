package br.com.zitrus.authorizer.adapter.persistence.procedure;

import br.com.zitrus.authorizer.application.port.out.ProcedurePort;
import br.com.zitrus.authorizer.domain.Procedure;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Transactional
public class ProcedureAdapter implements ProcedurePort {

    private final ProcedureRepository procedureRepository;
    private final ProcedurePersistenceMapper procedurePersistenceMapper;

    @Override
    public Optional<Procedure> getByCodeAndAgeAndSex(Integer code, Integer age, String sex) {
        return procedureRepository.findByCodeAndAgeAndSex(code, age, sex).map(procedurePersistenceMapper::mapToDomain);
    }
}
