package br.com.zitrus.authorizer.adapter.persistence.procedureRequest;

import br.com.zitrus.authorizer.adapter.persistence.procedureRequest.ProcedureRequestJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcedureRequestRepository extends JpaRepository<ProcedureRequestJpaEntity, Long> {
}
