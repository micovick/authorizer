package br.com.zitrus.authorizer.adapter.persistence.procedure;

import br.com.zitrus.authorizer.domain.Procedure;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ProcedurePersistenceMapper {
    public ProcedureJpaEntity mapToJpaEntity(Procedure procedure) {
        ProcedureJpaEntity procedureJpaEntity = new ProcedureJpaEntity();
        procedureJpaEntity.setId(procedure.getId());
        procedureJpaEntity.setAge(procedure.getAge());
        procedureJpaEntity.setSex(procedure.getSex());
        procedureJpaEntity.setCode(procedure.getCode());
        procedureJpaEntity.setAllow(procedure.getAllow());

        return procedureJpaEntity;
    }

    public Procedure mapToDomain(ProcedureJpaEntity procedureJpaEntity) {
        Procedure procedure = new Procedure();
        procedure.setId(procedureJpaEntity.getId());
        procedure.setCode(procedureJpaEntity.getCode());
        procedure.setAge(procedureJpaEntity.getAge());
        procedure.setSex(procedureJpaEntity.getSex());
        procedure.setAllow(procedureJpaEntity.isAllow());

        return procedure;
    }

    public List<Procedure> mapToDomain(List<ProcedureJpaEntity> entities) {
        return entities.stream().map(this::mapToDomain).collect(Collectors.toList());
    }
}
