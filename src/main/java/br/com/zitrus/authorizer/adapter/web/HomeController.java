package br.com.zitrus.authorizer.adapter.web;

import br.com.zitrus.authorizer.application.port.in.CreateProcedureRequestUseCase;
import br.com.zitrus.authorizer.application.port.in.DeleteProcedureRequestUseCase;
import br.com.zitrus.authorizer.application.port.in.ListProcedureRequestUseCase;
import br.com.zitrus.authorizer.domain.ProcedureRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class HomeController {
    private final ListProcedureRequestUseCase listProcedureRequestUseCase;
    private final CreateProcedureRequestUseCase createProcedureRequestUseCase;
    private final DeleteProcedureRequestUseCase deleteProcedureRequestUseCase;

    @RequestMapping("/")
    public String view(Map<String, Object> model) {
        List<ProcedureRequest> requestProcedures = listProcedureRequestUseCase.listAll();
        model.put("requestProcedures", requestProcedures);
        return "index";
    }

    @GetMapping("/solicitar")
    public String addBookView(Map<String, Object> model) {
        model.put("procedureRequest", new ProcedureRequest());
        model.put("addSuccess", false);
        model.put("addError", false);
        return "request";
    }

    @PostMapping("/solicitar")
    public RedirectView create(@ModelAttribute("procedureRequest") ProcedureRequest procedureRequest, RedirectAttributes redirectAttributes) {
        final RedirectView redirectView = new RedirectView("/authorizer/solicitar");

        Optional<ProcedureRequest> procedureRequestCreated = createProcedureRequestUseCase.create(new CreateProcedureRequestUseCase.CreateProcedureRequestCommand(procedureRequest));

        if (procedureRequestCreated.isPresent()) {
            redirectAttributes.addFlashAttribute("addSuccess", true);
        } else {
            redirectAttributes.addFlashAttribute("addError", true);
        }

        return redirectView;
    }

    @PostMapping(value = "/remover/{id}")
    public RedirectView delete(@PathVariable Long id, Map<String, Object> model) {
        deleteProcedureRequestUseCase.delete(new DeleteProcedureRequestUseCase.DeleteProcedureRequestCommand(id));
        return new RedirectView("/authorizer/");
    }
}
