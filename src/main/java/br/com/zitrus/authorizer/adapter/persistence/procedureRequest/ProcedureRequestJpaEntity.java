package br.com.zitrus.authorizer.adapter.persistence.procedureRequest;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@RequiredArgsConstructor
@Entity(name = "ProcedureRequestJpaEntity")
@Table(name = "procedure_request")
public class ProcedureRequestJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer code;

    private Integer age;

    private String sex;

    private boolean allowed;
}
