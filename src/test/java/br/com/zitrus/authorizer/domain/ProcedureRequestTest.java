package br.com.zitrus.authorizer.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProcedureRequestTest {

    @Test
    void givenProcedureRequest_whenCreated_thenProcedureRequestRequiredFieldsPopulated() {
        ProcedureRequest procedureRequest = new ProcedureRequest();
        procedureRequest.setSex("M");
        procedureRequest.setAge(20);
        procedureRequest.setCode(1234);

        assertEquals("M", procedureRequest.getSex());
        assertEquals(20, procedureRequest.getAge());
        assertEquals(1234, procedureRequest.getCode());

    }
}
