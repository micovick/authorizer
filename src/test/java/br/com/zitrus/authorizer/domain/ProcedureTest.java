package br.com.zitrus.authorizer.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProcedureTest {
    @Test
    void givenProcedure_whenCreated_thenProcedureRequiredFieldsPopulated() {
        Procedure procedure = new Procedure();
        procedure.setSex("M");
        procedure.setAge(20);
        procedure.setCode(1234);
        procedure.setAllow(true);

        assertEquals("M", procedure.getSex());
        assertEquals(20, procedure.getAge());
        assertEquals(1234, procedure.getCode());
        assertTrue(procedure.getAllow());

    }
}
