package br.com.zitrus.authorizer.application.service;

import br.com.zitrus.authorizer.application.port.in.DeleteProcedureRequestUseCase;
import br.com.zitrus.authorizer.application.port.out.DeleteProcedureRequestPort;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

public class DeleteProcedureRequestServiceTest {
    private final DeleteProcedureRequestPort deleteProcedureRequestPort = Mockito.mock(DeleteProcedureRequestPort.class);

    private final DeleteProcedureRequestUseCase deleteProcedureRequestService = new DeleteProcedureRequestService(deleteProcedureRequestPort);


    @Test
    void givenProperData_thenDeleteRequestSuccess() {
        doReturn(true).when(deleteProcedureRequestPort).delete(1L);

        boolean result = deleteProcedureRequestService.delete(new DeleteProcedureRequestUseCase.DeleteProcedureRequestCommand(1L));

        assertTrue(result);
    }

}
