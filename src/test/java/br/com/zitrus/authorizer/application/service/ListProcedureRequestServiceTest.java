package br.com.zitrus.authorizer.application.service;

import br.com.zitrus.authorizer.application.port.in.ListProcedureRequestUseCase;
import br.com.zitrus.authorizer.application.port.out.ListProcedureRequestPort;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.BDDMockito.then;

public class ListProcedureRequestServiceTest {
    private final ListProcedureRequestPort listProcedureRequestPort = Mockito.mock(ListProcedureRequestPort.class);
    private final ListProcedureRequestUseCase listProcedureRequestService = new ListProcedureRequestService(listProcedureRequestPort);

    @Test
    void shouldProcedureRequestExist_WhenListFindByIdRequestDone(){
        listProcedureRequestService.getById(1L);

        then(listProcedureRequestPort).should().getById(1L);
    }

    @Test
    void shouldProcedureRequestExist_WhenListFindAllRequestDone() {
        listProcedureRequestService.listAll();

        then(listProcedureRequestPort).should().listAll();
    }
}
