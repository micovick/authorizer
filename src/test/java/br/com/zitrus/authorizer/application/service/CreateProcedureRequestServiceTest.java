package br.com.zitrus.authorizer.application.service;

import br.com.zitrus.authorizer.application.port.in.CreateProcedureRequestUseCase;
import br.com.zitrus.authorizer.application.port.out.CreateProcedureRequestPort;
import br.com.zitrus.authorizer.application.port.out.ProcedurePort;
import br.com.zitrus.authorizer.domain.ProcedureRequest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class CreateProcedureRequestServiceTest {
    private final CreateProcedureRequestPort createProcedureRequestPort = Mockito.mock(CreateProcedureRequestPort.class);
    private final ProcedurePort procedurePort = mock(ProcedurePort.class);

    private final CreateProcedureRequestUseCase createProcedureRequestService = new CreateProcedureRequestService(createProcedureRequestPort, procedurePort);

    @Test
    void giveProcedureRequest_whenCommandIssued_thenCreationRequested() {
        ProcedureRequest procedureRequest = new ProcedureRequest();
        procedureRequest.setSex("M");
        procedureRequest.setAge(20);
        procedureRequest.setCode(1234);

        Optional<ProcedureRequest> result = createProcedureRequestService.create(new CreateProcedureRequestUseCase.CreateProcedureRequestCommand(procedureRequest));

        then(createProcedureRequestPort).should().create(procedureRequest);
    }
}
