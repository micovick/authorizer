package br.com.zitrus.authorizer.adapter.persistence;

import br.com.zitrus.authorizer.adapter.persistence.procedureRequest.ProcedureRequestAdapter;
import br.com.zitrus.authorizer.adapter.persistence.procedureRequest.ProcedureRequestPersistenceMapper;
import br.com.zitrus.authorizer.domain.ProcedureRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Import({ProcedureRequestAdapter.class, ProcedureRequestPersistenceMapper.class})
public class ProcedureRequestAdapterTest {
    @Autowired
    private ProcedureRequestAdapter procedureRequestAdapter;

    @Test
    void injectedComponentsAreNotNull(){
        assertThat(procedureRequestAdapter).isNotNull();
    }

    @Test
    void givenProcedureRequest_whenCreate_thenProcedureRequestReturned(){
        ProcedureRequest procedureRequest = new ProcedureRequest();
        procedureRequest.setSex("M");
        procedureRequest.setAge(20);
        procedureRequest.setCode(1234);

        Optional<ProcedureRequest> result = procedureRequestAdapter.create(procedureRequest);

        assertTrue(result.isPresent());
        assertEquals("M", procedureRequest.getSex());
        assertEquals(20, procedureRequest.getAge());
        assertEquals(1234, procedureRequest.getCode());
    }

    @Test
    @Sql(statements = {
            "insert into procedure_request (id, code, age, sex) values (1, 1234, 10, 'M')"
    })
    void givenProcedureRequest_whenDeleted_thenTrue() {

        boolean result = procedureRequestAdapter.delete(1L);

        assertTrue(result);
    }

    @Test
    @Sql(statements = {
            "insert into procedure_request (id, code, age, sex) values (1, 1234, 10, 'M')"
    })
    void givenInsertedProcedureRequest_whenGetById_thenProcedureRequestReturned() {

        Optional<ProcedureRequest> result = procedureRequestAdapter.getById(1L);

        assertTrue(result.isPresent());
        assertEquals("M", result.get().getSex());
        assertEquals(10, result.get().getAge());
        assertEquals(1234, result.get().getCode());
    }

    @Test
    void givenNoProcedureRequest_whenDeleted_thenFalse() {
        boolean result = procedureRequestAdapter.delete(2L);

        assertFalse(result);
    }


    @Test
    @Sql(statements = {
            "insert into procedure_request (id, code, age, sex) values (1, 1234, 10, 'M')",
            "insert into procedure_request (id, code, age, sex) values (2, 5678, 20, 'M')",
            "insert into procedure_request (id, code, age, sex) values (3, 7891, 40, 'F')",
            "insert into procedure_request (id, code, age, sex) values (4, 4562, 50, 'F')"
    })
    void givenInsertedProcedureRequest_whenFindAll_thenListReturned(){
        List<ProcedureRequest> result = procedureRequestAdapter.listAll();

        assertEquals(4, result.size());
    }

}
