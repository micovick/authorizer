package br.com.zitrus.authorizer.adapter.persistence;

import br.com.zitrus.authorizer.adapter.persistence.procedure.ProcedureAdapter;
import br.com.zitrus.authorizer.adapter.persistence.procedure.ProcedurePersistenceMapper;
import br.com.zitrus.authorizer.domain.Procedure;
import br.com.zitrus.authorizer.domain.ProcedureRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Import({ProcedureAdapter.class, ProcedurePersistenceMapper.class})
public class ProcedureAdapterTest {
    @Autowired
    private ProcedureAdapter procedureAdapter;

    @Test
    void injectedComponentsAreNotNull(){
        assertThat(procedureAdapter).isNotNull();
    }

    @Test
    void givenInsertedProcedure_whenGetByCodeAndAgeAndSex_thenProcedureRequestReturned() {

        Optional<Procedure> result = procedureAdapter.getByCodeAndAgeAndSex(1234, 10, "M");

        assertTrue(result.isPresent());
        assertEquals("M", result.get().getSex());
        assertEquals(10, result.get().getAge());
        assertEquals(1234, result.get().getCode());
        assertFalse(result.get().getAllow());
    }
}
